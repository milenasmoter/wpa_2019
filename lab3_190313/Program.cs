﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace point_2D //przestrzen nazw
{
    class Program
    {
        static void Main()
        {
            Point my_point = new Point();

            //nie mozna napisac my_point.x = 5.5; bo x to jest zmienna prywatna
            //nie mozna tak przypisac wartosci!
            //ustawiamy wartosc iksa:
            my_point.setX(3.3);

            Console.WriteLine("Wsp. x punktu my_point " + my_point.getX());
            Console.WriteLine("Wsp. y punktu my_point " + my_point.getY());

            Console.WriteLine("------------------------------");

            //zrobic wyswietlenie punktu, ktore wprowadzimy z klawiatury
            // mamy pobrac liczby z klawiatury (iks i igrek), uzywamy potem funkcji uzytych w klasie
            //i jak swtorze ten obiekt to mamy je wyswietlic
            //STWORZYC NOWY OBIEKT, nie my_point, cos swojego wziac!

            /*
            Point nowy_punkt = new Point();
            Console.WriteLine("Podaj wsp. x punktu nowy_punkt ");
            double a = Convert.ToDouble(Console.ReadLine());
            nowy_punkt.setX(a);
            Console.WriteLine("Podaj wsp. y punktu nowy_punkt ");
            double b = Convert.ToDouble(Console.ReadLine());
            nowy_punkt.setY(b);

            Console.WriteLine("Wsp. x punktu my_point " + nowy_punkt.getX());
            Console.WriteLine("Wsp. y punktu my_point " + nowy_punkt.getY());
            */

            Point new_point = new Point();
            Console.WriteLine("Podaj wsp. x ");
            double tmp_x;
            if (Double.TryParse(Console.ReadLine(), out tmp_x))
                Console.WriteLine("Wprowadzono poprawne dane");
            else
            {
                Console.WriteLine("Wprowadzono niepoprawne dane");
                Console.WriteLine("Zostanie wprowadzona wartosc domyslna 0.0");
                tmp_x = 0.0;
            }

            Console.WriteLine("Podaj wsp. y ");
            double tmp_y;
            if (Double.TryParse(Console.ReadLine(), out tmp_y))
                Console.WriteLine("Wprowadzono poprawne dane");
            else
            {
                Console.WriteLine("Wprowadzono niepoprawne dane");
                Console.WriteLine("Zostanie wprowadzona wartosc domyslna 0.0");
                tmp_y = 0.0;
            }

            new_point.setX(tmp_x);
            new_point.setY(tmp_y);

            Console.WriteLine("Wsp. x punktu new_point " + new_point.getX());
            Console.WriteLine("Wsp. y punktu new_point " + new_point.getY());

            Console.WriteLine("------------------------------");

            Point good_point = new Point(tmp_x, tmp_y);

            Console.WriteLine("Wsp. x punktu " + good_point.getX());
            Console.WriteLine("Wsp. y punktu " + good_point.getY());


            Console.WriteLine("------------------------------");
            //l od linear
            Point l_point = new Point(tmp_y);

            Console.WriteLine("Wsp. x punktu " + l_point.getX());
            Console.WriteLine("Wsp. y punktu " + l_point.getY());


            Console.WriteLine("------------------------------");


            //sprawdzac czy punkt nalezy do cwiarkti ktora zostala podana
            //podajemy x , y, nr cwiartki i wtedy kontruktor ma utworzyc obiekt
            // jak sienie nie bedzie zgadazac to ma przyjac warttosci domyslne

            Console.WriteLine("Podaj numer cwiartki");
            int tmp_cw;
            if (Int32.TryParse(Console.ReadLine(), out tmp_cw))
                Console.WriteLine("Wprowadzono poprawne dane");
            else
            {
                Console.WriteLine("Wprowadzono niepoprawne dane");
                Console.WriteLine("Zostanie wprowadzona wartosc domyslna 1");
                tmp_cw = 1;
            }

            Point another_point = new Point(tmp_x, tmp_y, tmp_cw);

            Console.WriteLine("Wsp. x punktu another_point " + another_point.getX());
            Console.WriteLine("Wsp. y punktu another_point " + another_point.getY());


            Console.WriteLine("------------------------------");

            Console.WriteLine("Odległość pomiędzy my_point i new_point " +
                new_point.distance(my_point) ); //this.x bedzie pochodzilo z new_point, a _x bedzie pochodzilo z my_point 

            Console.ReadKey();
        }
    }

}
