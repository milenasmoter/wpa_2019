﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace telebook
{
    class Telebook
    {
        Person person;
        Address address;
        Workplace workplace;
        Contact contact;

        public Telebook()
        {
            Console.WriteLine("Działa konstruktor klasy Telebook bez argumentów ");
            person = new Person();
            address = new Address();
            workplace = new Workplace();
            contact = new Contact();
        }
        public Telebook(Person _person, Address _address, Workplace _workplace, Contact _contact)
        {
            Console.WriteLine("Działa konstruktor klasy Telebook z arg. ");
            person = _person;
            address = _address;
            workplace = _workplace;
            contact = _contact;
        }

        public Person Person
        {
            get { return person; }
            set { person = value; }
        }

        public Address Address
        {
            get { return address; }
            set { address = value; }
        }

        public Workplace Workplace
        {
            get { return workplace; }
            set { workplace = value; }
        }

        public Contact Contact
        {
            get { return contact; }
            set { contact = value; }
        }
    }
}
