﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace moje_okno
{
    public partial class my_window : Form
    {
        public my_window()
        {
            InitializeComponent();
        }

        private void przycisk_koniec_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void my_window_Load(object sender, EventArgs e)
        {

        }



        private void przycisk_zatwierdz_dane_Click(object sender, EventArgs e)
        {
            etykieta_dane.Text = pole_tekstowe_imie.Text + " " + pole_tekstowe_nazwisko.Text + "\n" + data_urodzenia.Value.ToShortDateString() + "\n" + maskedTextBox1.Text;
        }

        private void pole_tekstowe_imie_Click(object sender, EventArgs e)
        {
            pole_tekstowe_imie.Text = "";
        }

        private void pole_tekstowe_nazwisko_Click(object sender, EventArgs e)
        {
            pole_tekstowe_nazwisko.Text = "";
        }
    }
}
