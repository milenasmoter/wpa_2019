from PyQt5.QtWidgets import QApplication, QWidget
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QLabel, QGridLayout
from PyQt5.QtWidgets import QLineEdit, QPushButton, QHBoxLayout
from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtCore import Qt

class  Kalkulator(QWidget):
    def __init__(self):
        super().__init__()

        self.interfejs()

    def interfejs(self):

        etykieta1 = QLabel('Liczba1', self)
        etykieta2 = QLabel('Liczba2', self)
        etykieta3 = QLabel('Wynik', self)

        ukladT = QGridLayout()
        ukladT.addWidget(etykieta1, 0, 0)
        ukladT.addWidget(etykieta2, 1, 0)
        ukladT.addWidget(etykieta3, 3, 0)

        self.Liczba1Pole = QLineEdit()
        self.Liczba2Pole = QLineEdit()
        self.WynikPole = QLineEdit()

        self.WynikPole.setReadOnly(True)
        self.WynikPole.setToolTip('wpisz liczby i wybierz dzialanie')

        ukladT.addWidget(self.Liczba1Pole, 0, 1)
        ukladT.addWidget(self.Liczba2Pole, 1, 1)
        ukladT.addWidget(self.WynikPole, 3, 1)

        dodajBtn = QPushButton('+', self)
        odejmijBtn = QPushButton('-', self)
        mnozBtn = QPushButton('*', self)
        dzielBtn = QPushButton('/', self)
        koniecBtn = QPushButton('Koniec', self)
        zerujBtn = QPushButton('Zeruj', self)

        ukladH = QHBoxLayout()
        ukladH.addWidget(dodajBtn)
        ukladH.addWidget(odejmijBtn)
        ukladH.addWidget(mnozBtn)
        ukladH.addWidget(dzielBtn)
        ukladH.addWidget(zerujBtn)


        ukladT.addLayout(ukladH, 2, 0, 1, 4)
        ukladT.addWidget(koniecBtn, 4, 0, 1, 4)

        self.setLayout(ukladT)

        koniecBtn.clicked.connect(self.koniec)
        dodajBtn.clicked.connect(self.dzialanie)
        odejmijBtn.clicked.connect(self.dzialanie)
        mnozBtn.clicked.connect(self.dzialanie)
        dzielBtn.clicked.connect(self.dzialanie)
        #zerujBtn.clicked.connect(self.zeruj)

        self.setGeometry(100,100,300,200)
        self.setWindowTitle("Kalkulator")
        self.setWindowIcon(QIcon('kalkulator.png'))
        self.show()

    def dzialanie(self):

        nadawca = self.sender()

        try:
            liczba1 = float(self.Liczba1Pole.text())
            liczba2 = float(self.Liczba2Pole.text())
            wynik = ''

            if nadawca.text() == '+':
                wynik = liczba1 + liczba2
            elif nadawca.text() == '-':
                wynik = liczba1 - liczba2
            elif nadawca.text() == '*':
                wynik = liczba1 * liczba2
            elif nadawca.text() == '/':
                try:
                    wynik = round(liczba1/liczba2, 9)
                except ZeroDivisionError:
                    QMessageBox.critical(self, 'Blad', 'Dzielisz przez 0!', QMessageBox.Ok)

            self.WynikPole.setText(str(wynik))

        except ValueError:
            QMessageBox.warning(self, 'Blad', 'Bledne dane', QMessageBox.Ok)

    #def zeruj(self):



    def koniec(self):
            self.close()

    def closeEvent(self, event):
        odp = QMessageBox.question(self, 'Komunikat',
        'Czy na pewno koniec?', QMessageBox.Yes | QMessageBox.No,
                                       QMessageBox.No)
        if odp == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()

    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Escape():
            self.close()

    def keyPressEvent1(self, f):
        if f.key() == Qt.Key_Q():
            self.close()




import sys

app = QApplication(sys.argv)
okno = Kalkulator()

app.exec_()