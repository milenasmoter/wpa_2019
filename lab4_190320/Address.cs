﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace telebook
{
    class Address
    {
        string ulica;
        int nr_domu;
        int nr_mieszkania;
        string kod_pocztowy;
        string miasto;

        public Address()
        {
            Console.WriteLine("Działa konstruktor klasy Address bez argumentów ");
            ulica = "Domyslna";
            nr_domu = 1;
            nr_mieszkania = 1;
            kod_pocztowy = "80-809";
            miasto = "Gdansk";
        }

        public Address(string _ulica, int _nr_domu, int _nr_mieszkania, string _kod_pocztowy, string _miasto)
        {
            Console.WriteLine("Działa konstruktor klasy Address z arg. ");
            ulica = _ulica; //name to nasza zmienna, a _name to to co podajemy
            nr_domu = _nr_domu;
            nr_mieszkania = _nr_mieszkania;
            kod_pocztowy = _kod_pocztowy;
            miasto = _miasto;
        }

        public string Ulica
        {
            get { return ulica; }
            set { ulica = value; }
        }

        public int Nr_domu
        {
            get { return nr_domu; }
            set { nr_domu = value; }
        }

        public int Nr_mieszkania
        {
            get { return nr_mieszkania; }
            set { nr_mieszkania = value; }
        }

        public string Kod_pocztowy
        {
            get { return kod_pocztowy; }
            set { kod_pocztowy = value; }
        }

        public string Miasto
        {
            get { return miasto; }
            set { miasto = value; }
        }




    }
}
