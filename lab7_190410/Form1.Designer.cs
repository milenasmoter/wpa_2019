﻿namespace Menu
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.my_menu = new System.Windows.Forms.MenuStrip();
            this.plikToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pokazOknoDialogoweToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.poleTekstoweToolStripTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.wyswietlTekstToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listaRozwijanaToolStripComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.zakonczToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pomocToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.opcja1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.opcja2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.opcja21ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.opcja22ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.opcja3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.opcjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oProgramieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.etykieta_odp = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.opcja1ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.oknoDialogoweToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.koniecToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.pole_Nazwisko = new System.Windows.Forms.TextBox();
            this.pole_Imie = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.my_menu.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // my_menu
            // 
            this.my_menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.plikToolStripMenuItem,
            this.pomocToolStripMenuItem,
            this.opcjeToolStripMenuItem});
            this.my_menu.Location = new System.Drawing.Point(0, 0);
            this.my_menu.Name = "my_menu";
            this.my_menu.Size = new System.Drawing.Size(851, 24);
            this.my_menu.TabIndex = 0;
            this.my_menu.Text = "menuStrip1";
            // 
            // plikToolStripMenuItem
            // 
            this.plikToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pokazOknoDialogoweToolStripMenuItem,
            this.poleTekstoweToolStripTextBox,
            this.toolStripSeparator1,
            this.wyswietlTekstToolStripMenuItem,
            this.listaRozwijanaToolStripComboBox,
            this.zakonczToolStripMenuItem});
            this.plikToolStripMenuItem.Name = "plikToolStripMenuItem";
            this.plikToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.plikToolStripMenuItem.Text = "Plik";
            // 
            // pokazOknoDialogoweToolStripMenuItem
            // 
            this.pokazOknoDialogoweToolStripMenuItem.Name = "pokazOknoDialogoweToolStripMenuItem";
            this.pokazOknoDialogoweToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.pokazOknoDialogoweToolStripMenuItem.Text = "Pokaż okno dialogowe";
            this.pokazOknoDialogoweToolStripMenuItem.Click += new System.EventHandler(this.pokazOknoDialogoweToolStripMenuItem_Click);
            // 
            // poleTekstoweToolStripTextBox
            // 
            this.poleTekstoweToolStripTextBox.Name = "poleTekstoweToolStripTextBox";
            this.poleTekstoweToolStripTextBox.Size = new System.Drawing.Size(100, 23);
            this.poleTekstoweToolStripTextBox.TextChanged += new System.EventHandler(this.poleTekstoweToolStripTextBox_TextChanged);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(190, 6);
            // 
            // wyswietlTekstToolStripMenuItem
            // 
            this.wyswietlTekstToolStripMenuItem.Name = "wyswietlTekstToolStripMenuItem";
            this.wyswietlTekstToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.wyswietlTekstToolStripMenuItem.Text = "Wyświetl tekst";
            this.wyswietlTekstToolStripMenuItem.Click += new System.EventHandler(this.wyswietlTekstToolStripMenuItem_Click);
            // 
            // listaRozwijanaToolStripComboBox
            // 
            this.listaRozwijanaToolStripComboBox.Items.AddRange(new object[] {
            "zielony",
            "żółty",
            "czerwony"});
            this.listaRozwijanaToolStripComboBox.Name = "listaRozwijanaToolStripComboBox";
            this.listaRozwijanaToolStripComboBox.Size = new System.Drawing.Size(121, 23);
            this.listaRozwijanaToolStripComboBox.Text = "Wybierz kolor";
            this.listaRozwijanaToolStripComboBox.SelectedIndexChanged += new System.EventHandler(this.listaRozwijanaToolStripComboBox_SelectedIndexChanged);
            // 
            // zakonczToolStripMenuItem
            // 
            this.zakonczToolStripMenuItem.Name = "zakonczToolStripMenuItem";
            this.zakonczToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.zakonczToolStripMenuItem.Text = "Zakończ";
            this.zakonczToolStripMenuItem.Click += new System.EventHandler(this.zakonczToolStripMenuItem_Click);
            // 
            // pomocToolStripMenuItem
            // 
            this.pomocToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.opcja1ToolStripMenuItem,
            this.opcja2ToolStripMenuItem,
            this.opcja3ToolStripMenuItem});
            this.pomocToolStripMenuItem.Name = "pomocToolStripMenuItem";
            this.pomocToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.pomocToolStripMenuItem.Text = "Opcje";
            // 
            // opcja1ToolStripMenuItem
            // 
            this.opcja1ToolStripMenuItem.Name = "opcja1ToolStripMenuItem";
            this.opcja1ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.opcja1ToolStripMenuItem.Text = "Opcja 1";
            this.opcja1ToolStripMenuItem.Click += new System.EventHandler(this.wyswietlTekstToolStripMenuItem_Click);
            // 
            // opcja2ToolStripMenuItem
            // 
            this.opcja2ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.opcja21ToolStripMenuItem,
            this.opcja22ToolStripMenuItem});
            this.opcja2ToolStripMenuItem.Name = "opcja2ToolStripMenuItem";
            this.opcja2ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.opcja2ToolStripMenuItem.Text = "Opcja 2";
            this.opcja2ToolStripMenuItem.Click += new System.EventHandler(this.wyswietlTekstToolStripMenuItem_Click);
            // 
            // opcja21ToolStripMenuItem
            // 
            this.opcja21ToolStripMenuItem.Name = "opcja21ToolStripMenuItem";
            this.opcja21ToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.opcja21ToolStripMenuItem.Text = "Opcja 2 1";
            this.opcja21ToolStripMenuItem.Click += new System.EventHandler(this.wyswietlTekstToolStripMenuItem_Click);
            // 
            // opcja22ToolStripMenuItem
            // 
            this.opcja22ToolStripMenuItem.Name = "opcja22ToolStripMenuItem";
            this.opcja22ToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.opcja22ToolStripMenuItem.Text = "Opcja 2 2";
            this.opcja22ToolStripMenuItem.Click += new System.EventHandler(this.wyswietlTekstToolStripMenuItem_Click);
            // 
            // opcja3ToolStripMenuItem
            // 
            this.opcja3ToolStripMenuItem.Name = "opcja3ToolStripMenuItem";
            this.opcja3ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.opcja3ToolStripMenuItem.Text = "Opcja 3";
            this.opcja3ToolStripMenuItem.Click += new System.EventHandler(this.wyswietlTekstToolStripMenuItem_Click);
            // 
            // opcjeToolStripMenuItem
            // 
            this.opcjeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.oProgramieToolStripMenuItem});
            this.opcjeToolStripMenuItem.Name = "opcjeToolStripMenuItem";
            this.opcjeToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.opcjeToolStripMenuItem.Text = "Pomoc";
            // 
            // oProgramieToolStripMenuItem
            // 
            this.oProgramieToolStripMenuItem.Name = "oProgramieToolStripMenuItem";
            this.oProgramieToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.oProgramieToolStripMenuItem.Text = "O programie";
            // 
            // etykieta_odp
            // 
            this.etykieta_odp.AutoSize = true;
            this.etykieta_odp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.etykieta_odp.Location = new System.Drawing.Point(254, 75);
            this.etykieta_odp.Name = "etykieta_odp";
            this.etykieta_odp.Size = new System.Drawing.Size(233, 20);
            this.etykieta_odp.TabIndex = 1;
            this.etykieta_odp.Text = "Tu będą pojawiać się informacje";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.opcja1ToolStripMenuItem1,
            this.oknoDialogoweToolStripMenuItem,
            this.koniecToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(162, 70);
            // 
            // opcja1ToolStripMenuItem1
            // 
            this.opcja1ToolStripMenuItem1.Name = "opcja1ToolStripMenuItem1";
            this.opcja1ToolStripMenuItem1.Size = new System.Drawing.Size(161, 22);
            this.opcja1ToolStripMenuItem1.Text = "Opcja 1";
            this.opcja1ToolStripMenuItem1.Click += new System.EventHandler(this.wyswietlTekstToolStripMenuItem_Click);
            // 
            // oknoDialogoweToolStripMenuItem
            // 
            this.oknoDialogoweToolStripMenuItem.Name = "oknoDialogoweToolStripMenuItem";
            this.oknoDialogoweToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.oknoDialogoweToolStripMenuItem.Text = "Okno dialogowe";
            this.oknoDialogoweToolStripMenuItem.Click += new System.EventHandler(this.pokazOknoDialogoweToolStripMenuItem_Click);
            // 
            // koniecToolStripMenuItem
            // 
            this.koniecToolStripMenuItem.Name = "koniecToolStripMenuItem";
            this.koniecToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.koniecToolStripMenuItem.Text = "Koniec";
            this.koniecToolStripMenuItem.Click += new System.EventHandler(this.zakonczToolStripMenuItem_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripComboBox1,
            this.toolStripTextBox1,
            this.toolStripButton2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(851, 25);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.Click += new System.EventHandler(this.zakonczToolStripMenuItem_Click);
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(121, 25);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 25);
            this.toolStripTextBox1.TextChanged += new System.EventHandler(this.poleTekstoweToolStripTextBox_TextChanged);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "toolStripButton2";
            // 
            // pole_Nazwisko
            // 
            this.pole_Nazwisko.Location = new System.Drawing.Point(418, 188);
            this.pole_Nazwisko.Name = "pole_Nazwisko";
            this.pole_Nazwisko.Size = new System.Drawing.Size(248, 20);
            this.pole_Nazwisko.TabIndex = 4;
            // 
            // pole_Imie
            // 
            this.pole_Imie.Location = new System.Drawing.Point(418, 142);
            this.pole_Imie.Name = "pole_Imie";
            this.pole_Imie.Size = new System.Drawing.Size(248, 20);
            this.pole_Imie.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(529, 248);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(851, 328);
            this.ContextMenuStrip = this.contextMenuStrip1;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pole_Imie);
            this.Controls.Add(this.pole_Nazwisko);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.etykieta_odp);
            this.Controls.Add(this.my_menu);
            this.MainMenuStrip = this.my_menu;
            this.Name = "Form1";
            this.Text = "Form1";
            this.my_menu.ResumeLayout(false);
            this.my_menu.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip my_menu;
        private System.Windows.Forms.ToolStripMenuItem plikToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pokazOknoDialogoweToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wyswietlTekstToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zakonczToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pomocToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem opcja1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem opcja2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem opcja21ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem opcja22ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem opcja3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem opcjeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oProgramieToolStripMenuItem;
        private System.Windows.Forms.Label etykieta_odp;
        private System.Windows.Forms.ToolStripTextBox poleTekstoweToolStripTextBox;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripComboBox listaRozwijanaToolStripComboBox;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem opcja1ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem oknoDialogoweToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem koniecToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.TextBox pole_Nazwisko;
        private System.Windows.Forms.TextBox pole_Imie;
        private System.Windows.Forms.Button button1;
    }
}

