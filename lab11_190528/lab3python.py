# import numpy as np
#
# import time
#
# print(time.asctime())
# print(time.time())
# t = time.time()
#
# def fib(n):
#     if n==0:
#         return 0
#     elif n==1:
#         return 1
#     elif n>1:
#         return fib(n-1) + fib(n-2)
#
# print(fib(30))
# print(time.time() - t)
#
#--------------------------------------------------------
# import sys
# print(sys.version)
# print(sys.argv[0])
# print('Hello {}'.format(sys.argv[1]))
#
# a = int(input('Podaj wyraz ciagu: '))
# print(fib(a))
#-------------------------------------------------------

# import os
# print(os.getcwd())
#jak chcemy zmienic sciezke pliku:
# path = 'C:/Users/student/PycharmProjects/lab3'
# os.chdir(path)
# print(os.listdir(path))
# print([plik for plik in os.listdir(path) if plik.endswith('pdb')])
#os.mkdir('nowy_folder')
# os.system('copy 3c5f.pdb copy3c5f.pdb')
# os.rename('copy3c5f.pdb', 'new3c5f.pdb')
# os.remove('new3c5f.pdb')

#---------------------------------------------------------
# plik = open('3c5f.pdb', 'r')
#
# f = plik.read()
# #print(f)
# plik2 = open('3c5f.pdb', 'r')
# g = plik.readlines()
# print(g)
# plik2.close()

#-------------------------------------------------------
# fhand = open('3c5f.pdb', 'r')
#
# count = 0
# for line in fhand:
#     if line.startswith('ATOM'):
#         count = count + 1
# fhand.close()
# print('W pliku {} znajduje sie {} linijek ATOM'.format(fhand, count))

#-----------------------------------------------------
# fhand = open('3c5f.pdb', 'r')
# f = fhand.readlines()
# seq = [l for l in f if l.startswith('SEQRES') and l.split()[2] == 'A']
# #print(seq[0][19:])
# print(seq)
# print(' '.join(seq))

#---------------------------------
# plik = open('opt_gaussian.log', 'r')
# f = plik.readlines()
# En = [float(l.split()[4]) for l in f if l.strip().startswith('SCF Done')]
# print(En)
#
# Enorm = [(e - En[-1]) for e in En]
# print(Enorm)
#
# outfile = open('output.out', 'w')
# for i in range(len(Enorm)):
#     outfile.write('{} {} \n'.format(i,Enorm[i]))
# outfile.close()

#------------------------------------
# import numpy as np
#
# print(np.array([1,2,3,4]))
# print(np.arange(10))
# print(np.arange(2,12,2))
#
# m = np.arange(20).reshape(4,5)
# print(m)
# print(m[0,3])
# print(m[1:3,1:3])
# print(m[:,-1])
# print(m.shape)
#
# print(np.zeros((2,3)))
# print(np.ones((2,3)))
#
# a = np.array(range(5),dtype=np.float)
# b = np.array(range(5,10),dtype=np.float)
# print(a+b)
#
# c = np.matrix(range(5),dtype=np.float)
# d = np.matrix(range(5,10),dtype=np.float)
#
# print(d.T)
# print(c*d.T)
#
# e = np.matrix(np.random.random((4,4)))
# print(e)

#------------------------------------------------------------
import numpy as np
dane = np.loadtxt('dih_sample.dat')
print(dane)

print(dane.shape)

dane1 = dane.reshape(10000,4)
sc = np.zeros((10000,8))

sc[:,::2] = np.sin(dane1)
sc[:,1::2] = np.cos(dane1)

print(dane1[0,:])
print(sc[0:])

dane_t0 = np.concatenate((dane, np.zeros(1)))
dane_t1 = np.concatenate((np.zeros(1),dane))

diff = dane_t1 - dane_t0
print(diff)