﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Menu
{
    public partial class Form1 : Form
    {
        public static string imie = "";
        public static string nazwisko = "";

        public Form1()
        {
            InitializeComponent();
        }

        private void zakonczToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void pokazOknoDialogoweToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult odp = MessageBox.Show("Czy dzisiaj jest ładna pogoda?", "Pytanie", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            switch(odp)
            {
                case DialogResult.Yes:
                    etykieta_odp.Text = "To świetnie :)";
                    break;
                case DialogResult.No:
                    etykieta_odp.Text = "To warto ubrać się ciepło.";
                    break;
            }

            //etykieta_odp.Text = odp.ToString();
        }

        private void wyswietlTekstToolStripMenuItem_Click(object sender, EventArgs e)
        {
            etykieta_odp.Text = ((ToolStripMenuItem)sender).Text;
        }

         

        private void listaRozwijanaToolStripComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //etykieta_odp.Text = listaRozwijanaToolStripComboBox.Text;

            if (listaRozwijanaToolStripComboBox.Text == "zielony")
            {
                this.BackColor = Color.Green;
            }
            else if (listaRozwijanaToolStripComboBox.Text == "żółty")
            {
                this.BackColor = Color.Yellow;
            }
            else if (listaRozwijanaToolStripComboBox.Text == "czerwony")
            {
                this.BackColor = Color.Red;
            }
        }

        private void poleTekstoweToolStripTextBox_TextChanged(object sender, EventArgs e)
        {
            etykieta_odp.Text = ((ToolStripTextBox)sender).Text;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            imie = pole_Imie.Text;
            nazwisko = pole_Nazwisko.Text;

            Form2 my_form2 = new Form2();
            my_form2.Show();
        }
    }
}
