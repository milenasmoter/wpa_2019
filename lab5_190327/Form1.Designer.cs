﻿namespace moje_okno
{
    partial class my_window
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.przycisk_koniec = new System.Windows.Forms.Button();
            this.nazwa_aplikacji = new System.Windows.Forms.Label();
            this.pole_tekstowe_imie = new System.Windows.Forms.TextBox();
            this.pole_tekstowe_nazwisko = new System.Windows.Forms.TextBox();
            this.etykieta_imie = new System.Windows.Forms.Label();
            this.etykieta_nazwisko = new System.Windows.Forms.Label();
            this.przycisk_zatwierdz_dane = new System.Windows.Forms.Button();
            this.etykieta_dane = new System.Windows.Forms.Label();
            this.pomoc_imie = new System.Windows.Forms.ToolTip(this.components);
            this.pomoc_nazwisko = new System.Windows.Forms.ToolTip(this.components);
            this.pomoc_zatwierdz_dane = new System.Windows.Forms.ToolTip(this.components);
            this.data_urodzenia = new System.Windows.Forms.DateTimePicker();
            this.etykieta_data_urodzenia = new System.Windows.Forms.Label();
            this.maskedTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.SuspendLayout();
            // 
            // przycisk_koniec
            // 
            this.przycisk_koniec.BackColor = System.Drawing.Color.Bisque;
            this.przycisk_koniec.Font = new System.Drawing.Font("Viner Hand ITC", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.przycisk_koniec.ForeColor = System.Drawing.Color.Crimson;
            this.przycisk_koniec.Location = new System.Drawing.Point(267, 397);
            this.przycisk_koniec.Name = "przycisk_koniec";
            this.przycisk_koniec.Size = new System.Drawing.Size(105, 53);
            this.przycisk_koniec.TabIndex = 0;
            this.przycisk_koniec.Text = "Koniec";
            this.przycisk_koniec.UseVisualStyleBackColor = false;
            this.przycisk_koniec.Click += new System.EventHandler(this.przycisk_koniec_Click);
            // 
            // nazwa_aplikacji
            // 
            this.nazwa_aplikacji.AutoSize = true;
            this.nazwa_aplikacji.Font = new System.Drawing.Font("Viner Hand ITC", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nazwa_aplikacji.Location = new System.Drawing.Point(55, 9);
            this.nazwa_aplikacji.Name = "nazwa_aplikacji";
            this.nazwa_aplikacji.Size = new System.Drawing.Size(256, 39);
            this.nazwa_aplikacji.TabIndex = 1;
            this.nazwa_aplikacji.Text = "Aplikacja TeleBook";
            // 
            // pole_tekstowe_imie
            // 
            this.pole_tekstowe_imie.Location = new System.Drawing.Point(101, 91);
            this.pole_tekstowe_imie.Name = "pole_tekstowe_imie";
            this.pole_tekstowe_imie.Size = new System.Drawing.Size(223, 20);
            this.pole_tekstowe_imie.TabIndex = 2;
            this.pole_tekstowe_imie.Text = "Wprowadź swoje imię";
            this.pomoc_imie.SetToolTip(this.pole_tekstowe_imie, "Kochany Użytkowniku, w tym polu wprowadź proszę swoje imię ");
            this.pole_tekstowe_imie.Click += new System.EventHandler(this.pole_tekstowe_imie_Click);
            // 
            // pole_tekstowe_nazwisko
            // 
            this.pole_tekstowe_nazwisko.Location = new System.Drawing.Point(101, 127);
            this.pole_tekstowe_nazwisko.Name = "pole_tekstowe_nazwisko";
            this.pole_tekstowe_nazwisko.Size = new System.Drawing.Size(223, 20);
            this.pole_tekstowe_nazwisko.TabIndex = 3;
            this.pole_tekstowe_nazwisko.Text = "Wprowadź swoje nazwisko";
            this.pomoc_nazwisko.SetToolTip(this.pole_tekstowe_nazwisko, "Kochany Użytkowniku, w tym polu wprowadź proszę swoje nazwisko");
            this.pole_tekstowe_nazwisko.Click += new System.EventHandler(this.pole_tekstowe_nazwisko_Click);
            // 
            // etykieta_imie
            // 
            this.etykieta_imie.AutoSize = true;
            this.etykieta_imie.Location = new System.Drawing.Point(56, 94);
            this.etykieta_imie.Name = "etykieta_imie";
            this.etykieta_imie.Size = new System.Drawing.Size(26, 13);
            this.etykieta_imie.TabIndex = 4;
            this.etykieta_imie.Text = "Imię";
            // 
            // etykieta_nazwisko
            // 
            this.etykieta_nazwisko.AutoSize = true;
            this.etykieta_nazwisko.Location = new System.Drawing.Point(29, 130);
            this.etykieta_nazwisko.Name = "etykieta_nazwisko";
            this.etykieta_nazwisko.Size = new System.Drawing.Size(53, 13);
            this.etykieta_nazwisko.TabIndex = 5;
            this.etykieta_nazwisko.Text = "Nazwisko";
            // 
            // przycisk_zatwierdz_dane
            // 
            this.przycisk_zatwierdz_dane.Font = new System.Drawing.Font("Arial Narrow", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.przycisk_zatwierdz_dane.Location = new System.Drawing.Point(12, 397);
            this.przycisk_zatwierdz_dane.Name = "przycisk_zatwierdz_dane";
            this.przycisk_zatwierdz_dane.Size = new System.Drawing.Size(105, 53);
            this.przycisk_zatwierdz_dane.TabIndex = 6;
            this.przycisk_zatwierdz_dane.Text = "Zatwierdź dane";
            this.pomoc_zatwierdz_dane.SetToolTip(this.przycisk_zatwierdz_dane, "Kochany Użytkowaniku, w tym polu zatwierdź swoje dane");
            this.przycisk_zatwierdz_dane.UseVisualStyleBackColor = true;
            this.przycisk_zatwierdz_dane.Click += new System.EventHandler(this.przycisk_zatwierdz_dane_Click);
            // 
            // etykieta_dane
            // 
            this.etykieta_dane.AutoSize = true;
            this.etykieta_dane.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.etykieta_dane.Location = new System.Drawing.Point(43, 276);
            this.etykieta_dane.Name = "etykieta_dane";
            this.etykieta_dane.Size = new System.Drawing.Size(281, 20);
            this.etykieta_dane.TabIndex = 7;
            this.etykieta_dane.Text = "Tu pojawią się wprowadzone dane";
            // 
            // pomoc_imie
            // 
            this.pomoc_imie.ToolTipTitle = "Wprowadź imię";
            // 
            // pomoc_nazwisko
            // 
            this.pomoc_nazwisko.ToolTipTitle = "Wprowadź Nazwisko";
            // 
            // pomoc_zatwierdz_dane
            // 
            this.pomoc_zatwierdz_dane.ToolTipTitle = "Zatwierdź dane";
            // 
            // data_urodzenia
            // 
            this.data_urodzenia.Location = new System.Drawing.Point(101, 172);
            this.data_urodzenia.Name = "data_urodzenia";
            this.data_urodzenia.Size = new System.Drawing.Size(200, 20);
            this.data_urodzenia.TabIndex = 8;
            // 
            // etykieta_data_urodzenia
            // 
            this.etykieta_data_urodzenia.AutoSize = true;
            this.etykieta_data_urodzenia.Location = new System.Drawing.Point(3, 178);
            this.etykieta_data_urodzenia.Name = "etykieta_data_urodzenia";
            this.etykieta_data_urodzenia.Size = new System.Drawing.Size(79, 13);
            this.etykieta_data_urodzenia.TabIndex = 9;
            this.etykieta_data_urodzenia.Text = "Data urodzenia";
            // 
            // maskedTextBox1
            // 
            this.maskedTextBox1.Location = new System.Drawing.Point(101, 223);
            this.maskedTextBox1.Mask = "00-000";
            this.maskedTextBox1.Name = "maskedTextBox1";
            this.maskedTextBox1.Size = new System.Drawing.Size(100, 20);
            this.maskedTextBox1.TabIndex = 10;
            // 
            // my_window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ClientSize = new System.Drawing.Size(384, 462);
            this.Controls.Add(this.maskedTextBox1);
            this.Controls.Add(this.etykieta_data_urodzenia);
            this.Controls.Add(this.data_urodzenia);
            this.Controls.Add(this.etykieta_dane);
            this.Controls.Add(this.przycisk_zatwierdz_dane);
            this.Controls.Add(this.etykieta_nazwisko);
            this.Controls.Add(this.etykieta_imie);
            this.Controls.Add(this.pole_tekstowe_nazwisko);
            this.Controls.Add(this.pole_tekstowe_imie);
            this.Controls.Add(this.nazwa_aplikacji);
            this.Controls.Add(this.przycisk_koniec);
            this.Name = "my_window";
            this.Text = "Moje okno";
            this.Load += new System.EventHandler(this.my_window_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button przycisk_koniec;
        private System.Windows.Forms.Label nazwa_aplikacji;
        private System.Windows.Forms.TextBox pole_tekstowe_imie;
        private System.Windows.Forms.TextBox pole_tekstowe_nazwisko;
        private System.Windows.Forms.Label etykieta_imie;
        private System.Windows.Forms.Label etykieta_nazwisko;
        private System.Windows.Forms.Button przycisk_zatwierdz_dane;
        private System.Windows.Forms.Label etykieta_dane;
        private System.Windows.Forms.ToolTip pomoc_imie;
        private System.Windows.Forms.ToolTip pomoc_nazwisko;
        private System.Windows.Forms.ToolTip pomoc_zatwierdz_dane;
        private System.Windows.Forms.DateTimePicker data_urodzenia;
        private System.Windows.Forms.Label etykieta_data_urodzenia;
        private System.Windows.Forms.MaskedTextBox maskedTextBox1;
    }
}

