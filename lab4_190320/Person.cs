﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace telebook
{
    class Person
    {
        string name;
        string surname;
        Date birth_date;

        public Person()
        {
            Console.WriteLine("Działa konstruktor klasy Person bez argumentów ");
            name = "John";
            surname = "Doe";
            birth_date = new Date();
        }

        public Person(string _name, string _surname, Date _birth_date)
        {
            Console.WriteLine("Działa konstruktor klasy Person z arg. ");
            name = _name; //name to nasza zmienna, a _name to to co podajemy
            surname = _surname;
            birth_date = _birth_date;
        }

        public Person(string _name, string _surname, int _day, int _month, int _year)
        {
            Console.WriteLine("Działa konstruktor klasy Person z 5 argumentami ");
            name = _name; //name to nasza zmienna, a _name to to co podajemy
            surname = _surname;
            birth_date = new Date(_day, _month, _year);
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Surname
        {
            get { return surname; }
            set { surname = value; }
        }

        public Date Birth_date //zwraca typ data
        {
            get { return birth_date; }
            set { birth_date = value; }
        }

    }
}
