﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace telebook
{
    class Workplace
    {
        string company;
        Address address;
        Date hire_date;
        Contact contact;

        public Workplace()
        {
            Console.WriteLine("Działa konstruktor klasy Workplace bez argumentów ");
            company = "Company";
            address = new Address();
            hire_date = new Date();
            contact = new Contact();
        }

        public Workplace(string _company, Address _address, Date _hire_date, Contact _contact)
        {
            Console.WriteLine("Działa konstruktor klasy Workplace z arg. ");
            company = _company;
            address = _address;
            hire_date = _hire_date;
            contact = _contact;
        }

        public Workplace(string _company, Address _address, int _day, int _month, int _year, Contact _contact)
        {
            Console.WriteLine("Działa konstruktor klasy Workplace z 6 arg. ");
            company = _company;
            address = _address;
            hire_date = new Date(_day, _month, _year);
            contact = _contact;
        }

        public string Company
        {
            get { return company; }
            set { company = value; }
        }

        public Address Address
        {
            get { return address; }
            set { address = value; }
        }

        public Date Hire_date
        {
            get { return hire_date; }
            set { hire_date = value; }
        }

        public Contact Contact 
        {
            get { return contact; }
            set { contact = value; }
        }


    }
}
