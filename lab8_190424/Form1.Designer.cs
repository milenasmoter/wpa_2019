﻿namespace quiz
{
    partial class okno_quiz
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.etykieta_witaj = new System.Windows.Forms.Label();
            this.pytanie_jeden = new System.Windows.Forms.Label();
            this.mark_twain = new System.Windows.Forms.RadioButton();
            this.odpowiedzi1_groupbox = new System.Windows.Forms.GroupBox();
            this.jk_rowling = new System.Windows.Forms.RadioButton();
            this.jk_rolling = new System.Windows.Forms.RadioButton();
            this.collin_dann = new System.Windows.Forms.RadioButton();
            this.twojwynik = new System.Windows.Forms.Label();
            this.pytanie_dwa = new System.Windows.Forms.Label();
            this.textBox_sowa = new System.Windows.Forms.TextBox();
            this.pytanie_trzy = new System.Windows.Forms.Label();
            this.dom_combobox = new System.Windows.Forms.ComboBox();
            this.pytanie_cztery = new System.Windows.Forms.Label();
            this.odpowiedzi2_groupbox = new System.Windows.Forms.GroupBox();
            this.ron = new System.Windows.Forms.CheckBox();
            this.malfoy = new System.Windows.Forms.CheckBox();
            this.hermiona = new System.Windows.Forms.CheckBox();
            this.dean = new System.Windows.Forms.CheckBox();
            this.pytanie_piec = new System.Windows.Forms.Label();
            this.zatwierdz_przycisk = new System.Windows.Forms.Button();
            this.ile_lat = new System.Windows.Forms.NumericUpDown();
            this.etykieta_twojwynik = new System.Windows.Forms.Label();
            this.przycisk_koniec = new System.Windows.Forms.Button();
            this.odpowiedzi1_groupbox.SuspendLayout();
            this.odpowiedzi2_groupbox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ile_lat)).BeginInit();
            this.SuspendLayout();
            // 
            // etykieta_witaj
            // 
            this.etykieta_witaj.AutoSize = true;
            this.etykieta_witaj.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.etykieta_witaj.Location = new System.Drawing.Point(177, 22);
            this.etykieta_witaj.Name = "etykieta_witaj";
            this.etykieta_witaj.Size = new System.Drawing.Size(264, 20);
            this.etykieta_witaj.TabIndex = 0;
            this.etykieta_witaj.Text = "Sprawdź swoją wiedzę o Harrym Potterze!";
            // 
            // pytanie_jeden
            // 
            this.pytanie_jeden.AutoSize = true;
            this.pytanie_jeden.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pytanie_jeden.Location = new System.Drawing.Point(33, 77);
            this.pytanie_jeden.Name = "pytanie_jeden";
            this.pytanie_jeden.Size = new System.Drawing.Size(251, 20);
            this.pytanie_jeden.TabIndex = 1;
            this.pytanie_jeden.Text = "1. Kto napisał serię o Harrym Potterze?";
            // 
            // mark_twain
            // 
            this.mark_twain.AutoSize = true;
            this.mark_twain.Location = new System.Drawing.Point(6, 19);
            this.mark_twain.Name = "mark_twain";
            this.mark_twain.Size = new System.Drawing.Size(81, 17);
            this.mark_twain.TabIndex = 2;
            this.mark_twain.TabStop = true;
            this.mark_twain.Text = "Mark Twain";
            this.mark_twain.UseVisualStyleBackColor = true;
            // 
            // odpowiedzi1_groupbox
            // 
            this.odpowiedzi1_groupbox.Controls.Add(this.collin_dann);
            this.odpowiedzi1_groupbox.Controls.Add(this.jk_rolling);
            this.odpowiedzi1_groupbox.Controls.Add(this.jk_rowling);
            this.odpowiedzi1_groupbox.Controls.Add(this.mark_twain);
            this.odpowiedzi1_groupbox.Location = new System.Drawing.Point(48, 109);
            this.odpowiedzi1_groupbox.Name = "odpowiedzi1_groupbox";
            this.odpowiedzi1_groupbox.Size = new System.Drawing.Size(236, 118);
            this.odpowiedzi1_groupbox.TabIndex = 3;
            this.odpowiedzi1_groupbox.TabStop = false;
            this.odpowiedzi1_groupbox.Text = "Odpowiedzi:";
            // 
            // jk_rowling
            // 
            this.jk_rowling.AutoSize = true;
            this.jk_rowling.Location = new System.Drawing.Point(6, 43);
            this.jk_rowling.Name = "jk_rowling";
            this.jk_rowling.Size = new System.Drawing.Size(87, 17);
            this.jk_rowling.TabIndex = 3;
            this.jk_rowling.TabStop = true;
            this.jk_rowling.Text = "J. K. Rowling";
            this.jk_rowling.UseVisualStyleBackColor = true;
            // 
            // jk_rolling
            // 
            this.jk_rolling.AutoSize = true;
            this.jk_rolling.Location = new System.Drawing.Point(7, 67);
            this.jk_rolling.Name = "jk_rolling";
            this.jk_rolling.Size = new System.Drawing.Size(81, 17);
            this.jk_rolling.TabIndex = 4;
            this.jk_rolling.TabStop = true;
            this.jk_rolling.Text = "J. K. Rolling";
            this.jk_rolling.UseVisualStyleBackColor = true;
            // 
            // collin_dann
            // 
            this.collin_dann.AutoSize = true;
            this.collin_dann.Location = new System.Drawing.Point(7, 91);
            this.collin_dann.Name = "collin_dann";
            this.collin_dann.Size = new System.Drawing.Size(79, 17);
            this.collin_dann.TabIndex = 5;
            this.collin_dann.TabStop = true;
            this.collin_dann.Text = "Collin Dann";
            this.collin_dann.UseVisualStyleBackColor = true;
            // 
            // twojwynik
            // 
            this.twojwynik.AutoSize = true;
            this.twojwynik.Location = new System.Drawing.Point(663, 586);
            this.twojwynik.Name = "twojwynik";
            this.twojwynik.Size = new System.Drawing.Size(34, 13);
            this.twojwynik.TabIndex = 4;
            this.twojwynik.Text = "Suma";
            // 
            // pytanie_dwa
            // 
            this.pytanie_dwa.AutoSize = true;
            this.pytanie_dwa.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pytanie_dwa.Location = new System.Drawing.Point(37, 249);
            this.pytanie_dwa.Name = "pytanie_dwa";
            this.pytanie_dwa.Size = new System.Drawing.Size(232, 20);
            this.pytanie_dwa.TabIndex = 5;
            this.pytanie_dwa.Text = "2. Jak nazywała się sowa Harry\'ego?";
            // 
            // textBox_sowa
            // 
            this.textBox_sowa.Location = new System.Drawing.Point(48, 284);
            this.textBox_sowa.Name = "textBox_sowa";
            this.textBox_sowa.Size = new System.Drawing.Size(221, 20);
            this.textBox_sowa.TabIndex = 6;
            // 
            // pytanie_trzy
            // 
            this.pytanie_trzy.AutoSize = true;
            this.pytanie_trzy.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pytanie_trzy.Location = new System.Drawing.Point(33, 331);
            this.pytanie_trzy.Name = "pytanie_trzy";
            this.pytanie_trzy.Size = new System.Drawing.Size(344, 20);
            this.pytanie_trzy.TabIndex = 7;
            this.pytanie_trzy.Text = "3. Do jakiego domu w Hogwarcie należał Harry Potter?";
            // 
            // dom_combobox
            // 
            this.dom_combobox.FormattingEnabled = true;
            this.dom_combobox.Items.AddRange(new object[] {
            "Gryffindor",
            "Hufflepuff",
            "Ravenclaw",
            "Slytherin"});
            this.dom_combobox.Location = new System.Drawing.Point(41, 365);
            this.dom_combobox.Name = "dom_combobox";
            this.dom_combobox.Size = new System.Drawing.Size(121, 21);
            this.dom_combobox.TabIndex = 8;
            // 
            // pytanie_cztery
            // 
            this.pytanie_cztery.AutoSize = true;
            this.pytanie_cztery.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pytanie_cztery.Location = new System.Drawing.Point(497, 77);
            this.pytanie_cztery.Name = "pytanie_cztery";
            this.pytanie_cztery.Size = new System.Drawing.Size(411, 20);
            this.pytanie_cztery.TabIndex = 9;
            this.pytanie_cztery.Text = "4. Którzy z poniższych byli najbliższymi przyjacielami Harry\'ego?";
            // 
            // odpowiedzi2_groupbox
            // 
            this.odpowiedzi2_groupbox.Controls.Add(this.dean);
            this.odpowiedzi2_groupbox.Controls.Add(this.hermiona);
            this.odpowiedzi2_groupbox.Controls.Add(this.malfoy);
            this.odpowiedzi2_groupbox.Controls.Add(this.ron);
            this.odpowiedzi2_groupbox.Location = new System.Drawing.Point(520, 109);
            this.odpowiedzi2_groupbox.Name = "odpowiedzi2_groupbox";
            this.odpowiedzi2_groupbox.Size = new System.Drawing.Size(200, 118);
            this.odpowiedzi2_groupbox.TabIndex = 10;
            this.odpowiedzi2_groupbox.TabStop = false;
            this.odpowiedzi2_groupbox.Text = "Odpowiedzi:";
            // 
            // ron
            // 
            this.ron.AutoSize = true;
            this.ron.Location = new System.Drawing.Point(7, 19);
            this.ron.Name = "ron";
            this.ron.Size = new System.Drawing.Size(90, 17);
            this.ron.TabIndex = 0;
            this.ron.Text = "Ron Weasley";
            this.ron.UseVisualStyleBackColor = true;
            // 
            // malfoy
            // 
            this.malfoy.AutoSize = true;
            this.malfoy.Location = new System.Drawing.Point(7, 43);
            this.malfoy.Name = "malfoy";
            this.malfoy.Size = new System.Drawing.Size(89, 17);
            this.malfoy.TabIndex = 1;
            this.malfoy.Text = "Draco Malfoy";
            this.malfoy.UseVisualStyleBackColor = true;
            // 
            // hermiona
            // 
            this.hermiona.AutoSize = true;
            this.hermiona.Location = new System.Drawing.Point(7, 67);
            this.hermiona.Name = "hermiona";
            this.hermiona.Size = new System.Drawing.Size(112, 17);
            this.hermiona.TabIndex = 2;
            this.hermiona.Text = "Hermiona Granger";
            this.hermiona.UseVisualStyleBackColor = true;
            // 
            // dean
            // 
            this.dean.AutoSize = true;
            this.dean.Location = new System.Drawing.Point(7, 91);
            this.dean.Name = "dean";
            this.dean.Size = new System.Drawing.Size(93, 17);
            this.dean.TabIndex = 3;
            this.dean.Text = "Dean Thomas";
            this.dean.UseVisualStyleBackColor = true;
            // 
            // pytanie_piec
            // 
            this.pytanie_piec.AutoSize = true;
            this.pytanie_piec.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pytanie_piec.Location = new System.Drawing.Point(501, 249);
            this.pytanie_piec.Name = "pytanie_piec";
            this.pytanie_piec.Size = new System.Drawing.Size(367, 20);
            this.pytanie_piec.TabIndex = 11;
            this.pytanie_piec.Text = "5. Ile lat miały osoby rozpoczynające naukę w Hogwarcie?";
            // 
            // zatwierdz_przycisk
            // 
            this.zatwierdz_przycisk.Location = new System.Drawing.Point(587, 492);
            this.zatwierdz_przycisk.Name = "zatwierdz_przycisk";
            this.zatwierdz_przycisk.Size = new System.Drawing.Size(110, 43);
            this.zatwierdz_przycisk.TabIndex = 12;
            this.zatwierdz_przycisk.Text = "Zatwierdź";
            this.zatwierdz_przycisk.UseVisualStyleBackColor = true;
            this.zatwierdz_przycisk.Click += new System.EventHandler(this.zatwierdz_przycisk_Click);
            // 
            // ile_lat
            // 
            this.ile_lat.Location = new System.Drawing.Point(527, 284);
            this.ile_lat.Name = "ile_lat";
            this.ile_lat.Size = new System.Drawing.Size(120, 20);
            this.ile_lat.TabIndex = 13;
            // 
            // etykieta_twojwynik
            // 
            this.etykieta_twojwynik.AutoSize = true;
            this.etykieta_twojwynik.Location = new System.Drawing.Point(634, 557);
            this.etykieta_twojwynik.Name = "etykieta_twojwynik";
            this.etykieta_twojwynik.Size = new System.Drawing.Size(63, 13);
            this.etykieta_twojwynik.TabIndex = 14;
            this.etykieta_twojwynik.Text = "Twój wynik:";
            // 
            // przycisk_koniec
            // 
            this.przycisk_koniec.Location = new System.Drawing.Point(803, 492);
            this.przycisk_koniec.Name = "przycisk_koniec";
            this.przycisk_koniec.Size = new System.Drawing.Size(90, 43);
            this.przycisk_koniec.TabIndex = 15;
            this.przycisk_koniec.Text = "Zakończ test";
            this.przycisk_koniec.UseVisualStyleBackColor = true;
            this.przycisk_koniec.Click += new System.EventHandler(this.przycisk_koniec_Click);
            // 
            // okno_quiz
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Beige;
            this.ClientSize = new System.Drawing.Size(948, 645);
            this.Controls.Add(this.przycisk_koniec);
            this.Controls.Add(this.etykieta_twojwynik);
            this.Controls.Add(this.ile_lat);
            this.Controls.Add(this.zatwierdz_przycisk);
            this.Controls.Add(this.pytanie_piec);
            this.Controls.Add(this.odpowiedzi2_groupbox);
            this.Controls.Add(this.pytanie_cztery);
            this.Controls.Add(this.dom_combobox);
            this.Controls.Add(this.pytanie_trzy);
            this.Controls.Add(this.textBox_sowa);
            this.Controls.Add(this.pytanie_dwa);
            this.Controls.Add(this.twojwynik);
            this.Controls.Add(this.odpowiedzi1_groupbox);
            this.Controls.Add(this.pytanie_jeden);
            this.Controls.Add(this.etykieta_witaj);
            this.Name = "okno_quiz";
            this.Text = "Quiz o Harrym Potterze";
            this.odpowiedzi1_groupbox.ResumeLayout(false);
            this.odpowiedzi1_groupbox.PerformLayout();
            this.odpowiedzi2_groupbox.ResumeLayout(false);
            this.odpowiedzi2_groupbox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ile_lat)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label etykieta_witaj;
        private System.Windows.Forms.Label pytanie_jeden;
        private System.Windows.Forms.RadioButton mark_twain;
        private System.Windows.Forms.GroupBox odpowiedzi1_groupbox;
        private System.Windows.Forms.RadioButton collin_dann;
        private System.Windows.Forms.RadioButton jk_rolling;
        private System.Windows.Forms.RadioButton jk_rowling;
        private System.Windows.Forms.Label twojwynik;
        private System.Windows.Forms.Label pytanie_dwa;
        private System.Windows.Forms.TextBox textBox_sowa;
        private System.Windows.Forms.Label pytanie_trzy;
        private System.Windows.Forms.ComboBox dom_combobox;
        private System.Windows.Forms.Label pytanie_cztery;
        private System.Windows.Forms.GroupBox odpowiedzi2_groupbox;
        private System.Windows.Forms.CheckBox dean;
        private System.Windows.Forms.CheckBox hermiona;
        private System.Windows.Forms.CheckBox malfoy;
        private System.Windows.Forms.CheckBox ron;
        private System.Windows.Forms.Label pytanie_piec;
        private System.Windows.Forms.Button zatwierdz_przycisk;
        private System.Windows.Forms.NumericUpDown ile_lat;
        private System.Windows.Forms.Label etykieta_twojwynik;
        private System.Windows.Forms.Button przycisk_koniec;
    }
}

