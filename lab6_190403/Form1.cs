﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace kalkulator
{
    public partial class calc : Form
    {
        public calc()
        {
            InitializeComponent();
        }

        private void wlacz_funkcje_CheckedChanged(object sender, EventArgs e)
        {
            if(wlacz_funkcje.Checked == true)
            {
                dzialania_arytmetyczne.Enabled = false;
                info_number2.Enabled = false;
                number2.Enabled = false;
                funkcje.Enabled = true;
            }
            else
            {
                dzialania_arytmetyczne.Enabled = true;
                info_number2.Enabled = true;
                info_number2.Enabled = true;
                funkcje.Enabled = false;
            }
        }

        private void oblicz_Click(object sender, EventArgs e)
        {
            if (wlacz_funkcje.Checked == false)
            {

                double tmp_number1, tmp_number2;
                if (Double.TryParse(number1.Text, out tmp_number1) && Double.TryParse(number2.Text, out tmp_number2))
                {
                    if (dodawanie.Checked == true)
                    {
                        wynik.Text = (tmp_number1 + tmp_number2).ToString();
                    }

                    else if (odejmowanie.Checked == true)
                    {
                        wynik.Text = (tmp_number1 - tmp_number2).ToString();
                    }

                    else if (mnozenie.Checked == true)
                    {
                        wynik.Text = (tmp_number1 * tmp_number2).ToString();
                    }

                    else if (dzielenie.Checked == true)
                    {
                        // dorobic dla dzielenia na 0
                        wynik.Text = (tmp_number1 / tmp_number2).ToString();
                    }
                }
                else
                {
                    MessageBox.Show("Wprowadzono niepoprawne dane", "Błędne dane", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                
                
            }
            else
            {
                double tmp_number1;
                if (Double.TryParse(number1.Text, out tmp_number1))
                {
                    if (fun_sin.Checked == true)
                    {
                        wynik.Text = (Math.Sin(tmp_number1)).ToString();
                    }

                    else if (fun_cos.Checked == true)
                    {
                        wynik.Text = (Math.Cos(tmp_number1)).ToString();
                    }

                    else if (fun_exp.Checked == true)
                    {
                        wynik.Text = (Math.Exp(tmp_number1)).ToString();
                    }

                    else if (fun_ln.Checked == true)
                    {
                        wynik.Text = (Math.Log(tmp_number1)).ToString();
                        //pokombinowac cos z tym 0
                    }
                }
                else
                {
                    MessageBox.Show("Wprowadzono niepoprawne dane", "Błędne dane", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                
                
            }

        }
    }
}
