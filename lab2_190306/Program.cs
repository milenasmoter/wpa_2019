﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics; //biblioteka by byly zespolone (dodalismy przez references)

namespace trojmian_kwadratowy
{
    class Program
    {
        static void Main()
        {
            //Complex c1 = new Complex(13, 4);
           // Complex value = new Complex(12.5, -6.3);
            //Console.WriteLine(c1);
            //Console.WriteLine("{0} + {1}i", value.Real, value.Imaginary);

            Console.WriteLine("To jest program obliczający pierwiastki równania kwadratowego\n");

            //Wprowadzamy wspolczynnik A
            Console.WriteLine("Wprowadź wsp. A:");
            double a;
            a = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Wsp. A=" + a);

            //Wprowadzamy wspolczynnik B
            Console.WriteLine("Wprowadź wsp. B:");
            double b;
            b = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Wsp. B=" + b);

            //Wprowadzamy wspolczynnik C
            Console.WriteLine("Wprowadź wsp. C:");
            double c;
            c = double.Parse(Console.ReadLine());
            Console.WriteLine("Wsp. C=" + c);

            double delta = Math.Pow(b, 2) - 4 * a * c;

            Console.WriteLine("Delta wynosi " + delta);

            double x0, x1, x2;

            //dodajemy wszystkie przypadki gdy np bedzie rownanie liniowe (a nie kwadratowe)
            if (a == 0.0 && b == 0.0 && c == 0.0)
            {
                Console.WriteLine("Nieskonczenie wiele rozwiazan");
            }

            else if (a == 0.0 && b == 0.0)
            {
                Console.WriteLine("Brak rozwiazan, sprzecznosc");
            }

            else if (a == 0.0)
            {
                Console.WriteLine("Rownanie liniowe");
                x0 = (-b) / c;
                Console.WriteLine("Istnieje jedno rozwiazanie dla rownania liniowego: " + x0);
            }

            else
            {
                if (delta > 0.0)
                {
                    double deltap = Math.Sqrt(delta);
                    Console.WriteLine("Pierwiastek z delty wynosi: " + deltap);
                    Console.WriteLine("Istnieją dwa pierwiastki w dziedzinie liczb rzeczywistych ");
                    x1 = (-b - deltap) / 2 * a;
                    x2 = (-b + deltap) / 2 * a;
                    Console.WriteLine("Pierwiastki równania to " + x1 + " oraz " + x2);
                }

                else if (delta == 0.0)
                {
                    Console.WriteLine("Istnieje jeden pierwiastek podwójny");
                    x0 = (-b) / 2 * a;
                    Console.WriteLine("Podwójny pierwiastek x0 wynosi: " + x0);
                }
                //szukanie pierwiastków dla delty < 0
                else
                {
                    delta = (-1) * delta;
                    double deltaz = Math.Sqrt(delta);
                    double Re, Im;
                    Console.WriteLine("Istnieją dwa pierwiastki w dziedzinie liczb zespolonych");
                    Console.WriteLine("Pierwiastek z delty wynosi: " + (deltaz) + "i");
                    //czesc rzeczywista z pierwiastka
                    Re = (-b) / 2 * a;
                    //czesc urojona z pierwiastka
                    Im = (deltaz) / 2 * a;

                    //Complex c1 = new Complex(13, 4);
                    //Complex value = new Complex(12.5, -6.3);
                    //Console.WriteLine(c1);
                    //Console.WriteLine("{0} + {1}i", value.Real, value.Imaginary);

                    Complex liczba = new Complex(Re, Im);
                    Console.WriteLine("x1 wynosi: ");
                    Console.WriteLine("{0} + {1}i", liczba.Real, liczba.Imaginary);
                    Console.WriteLine("x2 wynosi: ");
                    Console.WriteLine("{0} - {1}i", liczba.Real, liczba.Imaginary);

                    //Console.WriteLine("Ta liczba: " + liczba);


                    //Console.WriteLine("x1 =" + (Re + "+" + Im + "i"));
                    //Console.WriteLine("x2 =" + (Re + "-" + Im + "i"));
                }
            }

            /*
            Console.WriteLine(a + b);
            Console.WriteLine(a + c);
            */



            Console.ReadKey();
        }
    }
}
