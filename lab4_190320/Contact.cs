﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace telebook
{
    class Contact
    {
        int telefon;
        string mail;

        public Contact()
        {
            Console.WriteLine("Działa konstruktor klasy Contact bez argumentów ");
            telefon = 000000000;
            mail = "braknazwy@gmail.com";
        }

        public Contact(int _telefon, string _mail)
        {
            Console.WriteLine("Działa konstruktor klasy Contact z arg. ");
            telefon = _telefon; //name to nasza zmienna, a _name to to co podajemy
            mail = _mail;
        }

        public int Telefon
        {
            get { return telefon; }
            set { telefon = value; }
        }

        public string Mail
        {
            get { return mail; }
            set { mail = value; }
        }
    }
}
