#-----definiowanie funkcji i klas---------

# def hello(kto='World'):
#     """
#     :param kto: funkcja wczytuje osobe i wita ja
#     :return:
#     """
#     print('Hello ' + kto)
#     return len(kto)
#
# help(hello)
#
# wynik = hello('Tomek')
# print(wynik)
# hello()
# imie = 'Bartek'
# hello(imie)


# def ocena(punkty):
#     if punkty<40:
#         return 2, 'nie zdales'
#     elif punkty<50:
#         return 3, 'zdales'
#     elif punkty<60:
#         return 4, 'zdales'
#     else:
#         return 5, 'zdales'
#
# wynik = ocena(50)
# print(wynik)
# oc, status = ocena(20)
# print(oc)
# print(status)


#------widocznosc zmiennych---------
# a,b,c,d = 1,2,3,4
# def f(a,b):
#     c = a*b
#     print(locals())
#     globals()['d'] = 6
#     print(globals())
#     return c, d
#
# # f(8,10)
# print(f(c,d))


# zeby nie dzielic przez 0
# def dziel(dzielnik, dzielna = 1.0):
#     return dzielna/float(dzielnik)
#
# dzielniki = [1, 5, 45, 34, 77, 0, 67]
# #print(dziel(dzielniki[5]))
#
# lista =[]
# for i in dzielniki:
#     try:
#         lista.append(dziel(i))
#     except ZeroDivisionError:
#         lista.append(-1)
#
# print(lista)



# def get_acess(*args, **kwargs):
#     print(args)
#     print(kwargs)
#
# get_acess(2,4,dalej=4, jeszcze=8)
#
# def pliki (rozszerzenie, *args):
#     for i in args:
#         print(i + rozszerzenie)
#
# pliki('.py', 'cw1', 'cw2')



#--------klasy-------------
class Osoba:
    def __init__(self, wiek, plec):
        self.wiek = wiek
        self.plec = plec
    def rename(self, imie):
        self.imie = imie
    def __repr__(self):
        try:
            return self.imie
        except AttributeError:
            return 'no name'

o1 = Osoba(21, 'k')
print(o1)
print(o1.plec)
o1.rename('Magda')
print(o1)

#elementy dziedziczenia w nawiasie jest
class Pacjent(Osoba):
    def appoint(self, godzina):
        self.godzina = godzina

p1 = Pacjent('24', 'm')
p2 = Pacjent('30', 'k')
p3 = Pacjent('50', 'k')
p4 = Pacjent('10', 'm')
p1.rename('Pawel')
p2.rename('Magda')
p3.rename('Ewa')
p4.rename('Piotr')
p1.appoint(11)
p2.appoint(13)
p3.appoint(10)
p4.appoint(17)

# print(p1.godzina)
# print(p1)
#
# pacjenci = [p1, p2, p3, p4]
# print(pacjenci)
# #sortowanie pacjentow po godzinie
# pacjenci.sort(key=lambda x: x.godzina)
# print(pacjenci)