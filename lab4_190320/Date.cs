﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace telebook
{
    class Date
    {
        int day;
        int month;
        int year;

        public Date()
        {
            Console.WriteLine("Działa konstruktor klasy Date bez argumentów ");
            this.day = 1;
            this.month = 1;
            this.year = 1900;
        }

        public Date(int _day, int _month, int _year)
        {
            Console.WriteLine("Działa konstruktor klasy Date z argumentami ");

            if (_year < 1900 || _year > 2020)
            {
                Console.WriteLine("Wprowadzono niepoprawne dane - poprawiam na 1900.");
                this.year = 1900;
            }
            else
            {
                this.year = _year;
            }


            if (_month <= 0 || _month > 12)
            {
                Console.WriteLine("Wprowadzono niepoprawne dane - poprawiam na styczeń.");
                this.month = 1;
            }
            else
            {
                this.month = _month;
            }



            if (_month == 1 || _month == 3 || _month == 5 || _month == 7 || _month == 8 || _month == 10 || _month == 12)
            {
                if (_day <= 0 || _day > 31)
                {
                    Console.WriteLine("Wprowadzono niepoprawne dane - poprawiam na 1.");
                    this.day = 1;
                }
                else
                {
                    this.day = _day;
                }
            }
            else if (_month == 2)
            {
                if ((_year % 4 == 0) && (_year % 100 != 0) || (_year % 400 == 0))
                {
                    if (_day <= 0 || _day > 29)
                    {
                        Console.WriteLine("Wprowadzono niepoprawne dane - poprawiam na 1.");
                        this.day = 1;
                    }
                    else
                    {
                        this.day = _day;
                    }
                }
                else
                {
                    if (_day <= 0 || _day > 28)
                    {
                        Console.WriteLine("Wprowadzono niepoprawne dane - poprawiam na 1.");
                        this.day = 1;
                    }
                    else
                    {
                        this.day = _day;
                    }
                }
            }
            else
            {
                if (_day <= 0 || _day > 30)
                {
                    Console.WriteLine("Wprowadzono niepoprawne dane - poprawiam na 1.");
                    this.day = 1;
                }
                else
                {
                    this.day = _day;
                }
            }
        }

        public int Day
        {
            get { return day; }
            set { day = value; }
        }

        public int Month
        {
            get { return month; }
            set
            {
                if (value <= 0 || value > 12)
                {
                    Console.WriteLine("Wprowadzono niepoprawne dane - poprawiam na styczeń.");
                    this.month = 1;
                }
                else
                {
                    this.month = value;
                }
            }
        }

        public int Year
        {
            get { return year; }
            set { year = value; }
        }

    }
}
