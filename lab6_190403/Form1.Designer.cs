﻿namespace kalkulator
{
    partial class calc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.number1 = new System.Windows.Forms.TextBox();
            this.number2 = new System.Windows.Forms.TextBox();
            this.info_number1 = new System.Windows.Forms.Label();
            this.info_number2 = new System.Windows.Forms.Label();
            this.dzialania_arytmetyczne = new System.Windows.Forms.GroupBox();
            this.dodawanie = new System.Windows.Forms.RadioButton();
            this.odejmowanie = new System.Windows.Forms.RadioButton();
            this.funkcje = new System.Windows.Forms.GroupBox();
            this.fun_sin = new System.Windows.Forms.RadioButton();
            this.fun_cos = new System.Windows.Forms.RadioButton();
            this.mnozenie = new System.Windows.Forms.RadioButton();
            this.dzielenie = new System.Windows.Forms.RadioButton();
            this.fun_exp = new System.Windows.Forms.RadioButton();
            this.fun_ln = new System.Windows.Forms.RadioButton();
            this.wlacz_funkcje = new System.Windows.Forms.CheckBox();
            this.oblicz = new System.Windows.Forms.Button();
            this.wynik = new System.Windows.Forms.Label();
            this.dzialania_arytmetyczne.SuspendLayout();
            this.funkcje.SuspendLayout();
            this.SuspendLayout();
            // 
            // number1
            // 
            this.number1.Location = new System.Drawing.Point(52, 79);
            this.number1.Name = "number1";
            this.number1.Size = new System.Drawing.Size(150, 20);
            this.number1.TabIndex = 0;
            // 
            // number2
            // 
            this.number2.Location = new System.Drawing.Point(254, 79);
            this.number2.Name = "number2";
            this.number2.Size = new System.Drawing.Size(150, 20);
            this.number2.TabIndex = 1;
            // 
            // info_number1
            // 
            this.info_number1.AutoSize = true;
            this.info_number1.Location = new System.Drawing.Point(107, 45);
            this.info_number1.Name = "info_number1";
            this.info_number1.Size = new System.Drawing.Size(44, 13);
            this.info_number1.TabIndex = 2;
            this.info_number1.Text = "Liczba1";
            // 
            // info_number2
            // 
            this.info_number2.AutoSize = true;
            this.info_number2.Location = new System.Drawing.Point(316, 45);
            this.info_number2.Name = "info_number2";
            this.info_number2.Size = new System.Drawing.Size(44, 13);
            this.info_number2.TabIndex = 3;
            this.info_number2.Text = "Liczba2";
            // 
            // dzialania_arytmetyczne
            // 
            this.dzialania_arytmetyczne.Controls.Add(this.dzielenie);
            this.dzialania_arytmetyczne.Controls.Add(this.mnozenie);
            this.dzialania_arytmetyczne.Controls.Add(this.odejmowanie);
            this.dzialania_arytmetyczne.Controls.Add(this.dodawanie);
            this.dzialania_arytmetyczne.Location = new System.Drawing.Point(52, 136);
            this.dzialania_arytmetyczne.Name = "dzialania_arytmetyczne";
            this.dzialania_arytmetyczne.Size = new System.Drawing.Size(150, 163);
            this.dzialania_arytmetyczne.TabIndex = 4;
            this.dzialania_arytmetyczne.TabStop = false;
            this.dzialania_arytmetyczne.Text = "Działania arytmetyczne";
            // 
            // dodawanie
            // 
            this.dodawanie.AutoSize = true;
            this.dodawanie.Location = new System.Drawing.Point(27, 20);
            this.dodawanie.Name = "dodawanie";
            this.dodawanie.Size = new System.Drawing.Size(31, 17);
            this.dodawanie.TabIndex = 0;
            this.dodawanie.TabStop = true;
            this.dodawanie.Text = "+";
            this.dodawanie.UseVisualStyleBackColor = true;
            // 
            // odejmowanie
            // 
            this.odejmowanie.AutoSize = true;
            this.odejmowanie.Location = new System.Drawing.Point(27, 48);
            this.odejmowanie.Name = "odejmowanie";
            this.odejmowanie.Size = new System.Drawing.Size(28, 17);
            this.odejmowanie.TabIndex = 1;
            this.odejmowanie.TabStop = true;
            this.odejmowanie.Text = "-";
            this.odejmowanie.UseVisualStyleBackColor = true;
            // 
            // funkcje
            // 
            this.funkcje.Controls.Add(this.fun_ln);
            this.funkcje.Controls.Add(this.fun_exp);
            this.funkcje.Controls.Add(this.fun_cos);
            this.funkcje.Controls.Add(this.fun_sin);
            this.funkcje.Enabled = false;
            this.funkcje.Location = new System.Drawing.Point(254, 136);
            this.funkcje.Name = "funkcje";
            this.funkcje.Size = new System.Drawing.Size(150, 163);
            this.funkcje.TabIndex = 5;
            this.funkcje.TabStop = false;
            this.funkcje.Text = "Funkcje";
            // 
            // fun_sin
            // 
            this.fun_sin.AutoSize = true;
            this.fun_sin.Location = new System.Drawing.Point(14, 20);
            this.fun_sin.Name = "fun_sin";
            this.fun_sin.Size = new System.Drawing.Size(49, 17);
            this.fun_sin.TabIndex = 0;
            this.fun_sin.TabStop = true;
            this.fun_sin.Text = "sinus";
            this.fun_sin.UseVisualStyleBackColor = true;
            // 
            // fun_cos
            // 
            this.fun_cos.AutoSize = true;
            this.fun_cos.Location = new System.Drawing.Point(14, 48);
            this.fun_cos.Name = "fun_cos";
            this.fun_cos.Size = new System.Drawing.Size(61, 17);
            this.fun_cos.TabIndex = 1;
            this.fun_cos.TabStop = true;
            this.fun_cos.Text = "cosinus";
            this.fun_cos.UseVisualStyleBackColor = true;
            // 
            // mnozenie
            // 
            this.mnozenie.AutoSize = true;
            this.mnozenie.Location = new System.Drawing.Point(26, 84);
            this.mnozenie.Name = "mnozenie";
            this.mnozenie.Size = new System.Drawing.Size(29, 17);
            this.mnozenie.TabIndex = 2;
            this.mnozenie.TabStop = true;
            this.mnozenie.Text = "*";
            this.mnozenie.UseVisualStyleBackColor = true;
            // 
            // dzielenie
            // 
            this.dzielenie.AutoSize = true;
            this.dzielenie.Location = new System.Drawing.Point(26, 117);
            this.dzielenie.Name = "dzielenie";
            this.dzielenie.Size = new System.Drawing.Size(30, 17);
            this.dzielenie.TabIndex = 3;
            this.dzielenie.TabStop = true;
            this.dzielenie.Text = "/";
            this.dzielenie.UseVisualStyleBackColor = true;
            // 
            // fun_exp
            // 
            this.fun_exp.AutoSize = true;
            this.fun_exp.Location = new System.Drawing.Point(14, 84);
            this.fun_exp.Name = "fun_exp";
            this.fun_exp.Size = new System.Drawing.Size(42, 17);
            this.fun_exp.TabIndex = 2;
            this.fun_exp.TabStop = true;
            this.fun_exp.Text = "exp";
            this.fun_exp.UseVisualStyleBackColor = true;
            // 
            // fun_ln
            // 
            this.fun_ln.AutoSize = true;
            this.fun_ln.Location = new System.Drawing.Point(14, 117);
            this.fun_ln.Name = "fun_ln";
            this.fun_ln.Size = new System.Drawing.Size(110, 17);
            this.fun_ln.TabIndex = 3;
            this.fun_ln.TabStop = true;
            this.fun_ln.Text = "logarytm naturalny";
            this.fun_ln.UseVisualStyleBackColor = true;
            // 
            // wlacz_funkcje
            // 
            this.wlacz_funkcje.AutoSize = true;
            this.wlacz_funkcje.Location = new System.Drawing.Point(232, 333);
            this.wlacz_funkcje.Name = "wlacz_funkcje";
            this.wlacz_funkcje.Size = new System.Drawing.Size(197, 17);
            this.wlacz_funkcje.TabIndex = 6;
            this.wlacz_funkcje.Text = "Zaznacz jeśli chcesz używać funkcji";
            this.wlacz_funkcje.UseVisualStyleBackColor = true;
            this.wlacz_funkcje.CheckedChanged += new System.EventHandler(this.wlacz_funkcje_CheckedChanged);
            // 
            // oblicz
            // 
            this.oblicz.Location = new System.Drawing.Point(232, 378);
            this.oblicz.Name = "oblicz";
            this.oblicz.Size = new System.Drawing.Size(146, 45);
            this.oblicz.TabIndex = 7;
            this.oblicz.Text = "Oblicz";
            this.oblicz.UseVisualStyleBackColor = true;
            this.oblicz.Click += new System.EventHandler(this.oblicz_Click);
            // 
            // wynik
            // 
            this.wynik.AutoSize = true;
            this.wynik.Location = new System.Drawing.Point(76, 356);
            this.wynik.Name = "wynik";
            this.wynik.Size = new System.Drawing.Size(99, 13);
            this.wynik.TabIndex = 8;
            this.wynik.Text = "Tu pojawi się wynik";
            // 
            // calc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 462);
            this.Controls.Add(this.wynik);
            this.Controls.Add(this.oblicz);
            this.Controls.Add(this.wlacz_funkcje);
            this.Controls.Add(this.funkcje);
            this.Controls.Add(this.dzialania_arytmetyczne);
            this.Controls.Add(this.info_number2);
            this.Controls.Add(this.info_number1);
            this.Controls.Add(this.number2);
            this.Controls.Add(this.number1);
            this.Name = "calc";
            this.Text = "Kalkulator";
            this.dzialania_arytmetyczne.ResumeLayout(false);
            this.dzialania_arytmetyczne.PerformLayout();
            this.funkcje.ResumeLayout(false);
            this.funkcje.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox number1;
        private System.Windows.Forms.TextBox number2;
        private System.Windows.Forms.Label info_number1;
        private System.Windows.Forms.Label info_number2;
        private System.Windows.Forms.GroupBox dzialania_arytmetyczne;
        private System.Windows.Forms.RadioButton dzielenie;
        private System.Windows.Forms.RadioButton mnozenie;
        private System.Windows.Forms.RadioButton odejmowanie;
        private System.Windows.Forms.RadioButton dodawanie;
        private System.Windows.Forms.GroupBox funkcje;
        private System.Windows.Forms.RadioButton fun_ln;
        private System.Windows.Forms.RadioButton fun_exp;
        private System.Windows.Forms.RadioButton fun_cos;
        private System.Windows.Forms.RadioButton fun_sin;
        private System.Windows.Forms.CheckBox wlacz_funkcje;
        private System.Windows.Forms.Button oblicz;
        private System.Windows.Forms.Label wynik;
    }
}

