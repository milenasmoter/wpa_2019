from lab2_python import *

class Queue:
    def __init__(self, p_list):
        self.patients = p_list
        self.patients.sort(key= lambda x: x.godzina)
    def add(self, new_pat):
        if isinstance(new_pat, Pacjent):
            self.patients.append(new_pat)
        elif isinstance(new_pat, list):
            self.patients.extend(new_pat)
        else:
            raise TypeError
        self.patients.sort(key=lambda x: x.godzina)
    def __len__(self):
        return len(self.patients)
    def __repr__(self):
        return 'w kolejce sa ' + ' '.join([str(a) for a in self.patients])

que = Queue([p1])
que.add([p2])
que.add([p3, p4])
print(que)
print('w kolejce jest ' + str(len(que)) + ' osob')