﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace point_2D
{
    class Point
    {
        //budujemy punkt - dodając atrybuty
        double x;
        double y;

        public Point()
        {
            Console.WriteLine("Działa konstruktor klasy Point bez argumentów ");
            this.x = 0.0;
            this.y = 0.0;
        }

        public Point(double tmp)
        {
            Console.WriteLine("Działa konstruktor klasy Point z jednym argumentem ");
            this.x = tmp;
            this.y = tmp;
        }
        public Point(double _x, double _y)
        {
            Console.WriteLine("Działa konstruktor klasy Point z argumentami ");
            this.x = _x;
            this.y = _y;
        }
        public Point(double _x, double _y, int cwiartka)
        {
            Console.WriteLine("Działa konstruktor klasy Point z  trzema argumentami ");
            if (cwiartka == 1 && _x >= 0.0 && _y >= 0.0)
            {
                Console.WriteLine("Punkt leży w ćwiartce numer " + cwiartka);
                this.x = _x;
                this.y = _y;
            }
            else if (cwiartka == 2 && _x < 0.0 && _y > 0.0)
            {
                Console.WriteLine("Punkt leży w ćwiartce numer " + cwiartka);
                this.x = _x;
                this.y = _y;
            }
            else if (cwiartka == 3 && _x < 0.0 && _y < 0.0)
            {
                Console.WriteLine("Punkt leży w ćwiartce numer " + cwiartka);
                this.x = _x;
                this.y = _y;
            }
            else if (cwiartka == 4 && _x > 0.0 && _y < 0.0)
            {
                Console.WriteLine("Punkt leży w ćwiartce numer " + cwiartka);
                this.x = _x;
                this.y = _y;
            }
            else
            {
                Console.WriteLine("Wprowadzono niepoprawne dane");
                Console.WriteLine("Obiekt zostanie zainicjowany wartoscia domyslna 0.0, 0.0");
                this.x = 0.0;
                this.y = 0.0;
            }
        }

        public void setX(double _x) { this.x = _x; }
        public void setY(double _y) { this.y = _y; }
        public double getX() { return this.x; }
        public double getY() { return this.y; }

        //jeden bedziemy mieli, a drugi podamy
        public double distance(Point p)
        {
            return Math.Sqrt(Math.Pow((this.x - p.x), 2)
                + Math.Pow((this.y - p.y), 2) );
        }

    }
}