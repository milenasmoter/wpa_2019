﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace telebook
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Podaj miesiac");
            int tmp_month;

            while (!Int32.TryParse(Console.ReadLine(), out tmp_month))
            {
                Console.WriteLine("Wprowadzono niepoprawne dane - popraw się :)");
            }

            Date my_date = new Date(12, tmp_month, 2000);

            Console.WriteLine("Dzień: " + my_date.Day); //tu zadziala getter

            my_date.Day = 22;
            Console.WriteLine("Dzień: " + my_date.Day); //tu zadziala setter

           // my_date.Month = 12;

            Console.WriteLine("Miesiąc: " + my_date.Month); //tu zadziala setter

            Console.WriteLine("-------------------------------------------------------------------------");

            Person my_person = new Person();

            Console.WriteLine("Rok urodzenia osoby my_person: " + my_person.Birth_date.Year);

            Console.WriteLine("-------------------------------------------------------------------------");

            Person new_person = new Person("Joanna", "Kowalska", my_date);

            Console.WriteLine("Imię osoby new_person: " + new_person.Name);
            Console.WriteLine("Rok urodzenia osoby new_person: " + new_person.Birth_date.Year);

            Console.WriteLine("-------------------------------------------------------------------------");

            Person next_person = new Person("Krystian", "Kowalski", 3, 4, 1999);

            Console.WriteLine("Imię osoby next_person: " + next_person.Name);
            Console.WriteLine("Rok urodzenia osoby next_person: " + next_person.Birth_date.Year);

            Console.WriteLine("-------------------------------------------------------------------------");

            Address my_address = new Address("Dworcowa", 12, 15, "80-809", "Gdansk");

            Console.WriteLine("Ulica w adresie my_address: " + my_address.Ulica);

            Console.WriteLine("-------------------------------------------------------------------------");

            Address new_address = new Address("Dworcowa", 12, 15, "80-809", "Gdansk");

            Console.WriteLine("Kod pocztowy w adresie new_address: " + new_address.Kod_pocztowy);

            Console.WriteLine("-------------------------------------------------------------------------");

            Contact new_contact = new Contact(666452968, "costam@gmail.com");

            Console.WriteLine("Telefon w kontakcie new_contact: " + new_contact.Telefon);

            Console.WriteLine("-------------------------------------------------------------------------");

            Workplace my_workplace = new Workplace(); //domyslne
            Console.WriteLine("Nazwa firmy my_workplace " + my_workplace.Company);

            Console.WriteLine("-------------------------------------------------------------------------");

            Console.WriteLine("Podaj nazwę ulicy:  ");
            string tmp_ulica;
            tmp_ulica = Console.ReadLine();
            Console.WriteLine("Podaj numer domu:  ");
            string tmp_nr_domu;
            tmp_nr_domu = Console.ReadLine();
            Console.WriteLine("Podaj numer mieszkania:  ");
            string tmp_nr_mieszkania;
            tmp_nr_mieszkania = Console.ReadLine();
            Console.WriteLine("Podaj kod pocztowy:  ");
            string tmp_kod_pocztowy;
            tmp_kod_pocztowy = Console.ReadLine();
            Console.WriteLine("Podaj miasto:  ");
            string tmp_miasto;
            tmp_miasto = Console.ReadLine();

            Console.WriteLine("-------------------------------------------------------------------------");

            Console.WriteLine("Podaj nr telefonu:  ");
            int tmp_telefon;
            while (!Int32.TryParse(Console.ReadLine(), out tmp_telefon))
            {
                Console.WriteLine("Wprowadzono niepoprawne dane - popraw się :)");
            }
            Console.WriteLine("Podaj mail:  ");
            string tmp_mail;
            tmp_mail = Console.ReadLine();

            Console.WriteLine("-------------------------------------------------------------------------");

            Contact nowy_contact = new Contact(tmp_telefon, tmp_mail);
            Console.WriteLine("Telefon nowy_contact: " + tmp_telefon + " oraz mail: " + tmp_mail);

            Console.ReadKey();
        }
    }
}
