from tkinter import *

import time
import datetime
import pygame
import tkinter
from tkinter import Tk, Frame, Checkbutton
from tkinter import BooleanVar, BOTH

pygame.init()
root = Tk()
root.title("Pianino")

root.geometry("1192x660")
root.configure(background="white")

#-----------warstwy-------------------

ABC = Frame(root, bg="navajowhite", bd=10, relief=RAISED)
ABC.grid()
ABC1 = Frame(ABC, bg="navajowhite")
ABC1.grid()
ABC2 = Frame(ABC, bg="navajowhite")
ABC2.grid()
ABC3 = Frame(ABC, bg="navajowhite")
ABC3.grid()

overdrive = IntVar()

str1 = StringVar()
str1.set("Graj myszką lub klawiaturą!")
Date1 = StringVar()
Time1 = StringVar()

Date1.set(time.strftime("%d/%m/%Y"))
Time1.set(time.strftime("%H:%M:%S"))

# -------------- obszar z nazwa -----------------------
Label(ABC1, text="Pianino", font=('arial', 25, 'bold'), padx=5, pady=5, bd=4, bg="navajowhite",
      fg="black", justify=LEFT).grid(row=0, column=0, columnspan=11)

# ---------------okienka w 1 warstwie---------------------------
txtDate = Entry(ABC1, textvariable=Date1, font=('arial', 18, 'bold'), bd=10, relief=GROOVE, bg="sienna",
                fg="black", width=28, justify=CENTER).grid(row=1, column=0, pady=5)

txtDisplay = Entry(ABC1, textvariable=str1, font=('arial', 14, 'bold'), bd=10, relief=FLAT, bg="navajowhite",
                   fg="black", width=28, justify=CENTER).grid(row=1, column=1, pady=2)

txtTime = Entry(ABC1, textvariable=Time1, font=('arial', 18, 'bold'), bd=10, relief=GROOVE, bg="sienna",
                fg="black", width=28, justify=CENTER).grid(row=1, column=2, pady=5)


# --------------------klasa1------------------------------------------------------------------

class Key1(Button):
    def __init__(self, column, name, frame, key, master):
        Button.__init__(self, frame, height=6, width=4, text=name, relief=RAISED, padx=5, pady=5,
                        font=('arial', 18, 'bold'), bd=5,
                        bg="black", fg="white", command=self.play)
        Button.grid(self, row=0, column=column, padx=5, pady=5)
        self.column = column
        self.name = name
        self.master = master
        self.sound = pygame.mixer.Sound("Music_Notes/{}.wav".format(name))
        self.sound_overdrive = pygame.mixer.Sound("Music_Notes/{}_Drum.wav".format(name))
        self.key = key
        self.master.bind(self.key, self.play)

    def play(self, _event=None):
        global overdrive
        if overdrive.get() == 0:
            self.sound.play()
        else:
            self.sound_overdrive.play()

#----------------definiowanie przycisku do przesteru--------------------------
overdrive.set(0)
przester = Checkbutton(ABC1, text="PRZESTER", bg="navajowhite", onvalue=1, offvalue=0, fg="black",
                       font=('arial', 10, 'bold'), variable=overdrive)
przester.grid(row=2, column=1)

btnCs = Key1(0, 'C_s', ABC2, 'w', root)
ABC2.grid_columnconfigure(1, minsize=25)
btnDs = Key1(2, 'D_s', ABC2, 'e', root)
ABC2.grid_columnconfigure(3, minsize=130)
btnFs = Key1(4, 'F_s', ABC2, 't', root)
ABC2.grid_columnconfigure(5, minsize=25)
btnGs = Key1(6, 'G_s', ABC2, 'y', root)
ABC2.grid_columnconfigure(7, minsize=25)
btnBb = Key1(8, 'Bb', ABC2, 'u', root)
ABC2.grid_columnconfigure(9, minsize=130)
btnCs1 = Key1(10, 'C_s1', ABC2, 'o', root)
ABC2.grid_columnconfigure(11, minsize=25)
btnDs1 = Key1(12, 'D_s1', ABC2, 'p', root)


# -----------------------------klasa2---------------------------------------------------------

class Key2(Button):
    def __init__(self, column, name, frame, key, master):
        Button.__init__(self, frame, height=8, width=6, text=name, relief=SUNKEN, font=('arial', 18, 'bold'), bd=5,
                        bg="white", fg="black", command=self.play)
        Button.grid(self, row=0, column=column, padx=5, pady=5)
        self.column = column
        self.name = name
        self.master = master
        self.sound = pygame.mixer.Sound("Music_Notes/{}.wav".format(name))
        self.sound_overdrive = pygame.mixer.Sound("Music_Notes/{}_Drum.wav".format(name))
        self.key = key
        self.master.bind(self.key, self.play)

    def play(self, _event=None):
        global overdrive
        if overdrive.get() == 0:
            self.sound.play()
        else:
            self.sound_overdrive.play()

btnC = Key2(0, 'C', ABC3, 'a', root)
btnD = Key2(1, 'D', ABC3, 's', root)
btnE = Key2(2, 'E', ABC3, 'd', root)
btnF = Key2(3, 'F', ABC3, 'f', root)
btnG = Key2(4, 'G', ABC3, 'g', root)
btnA = Key2(5, 'A', ABC3, 'h', root)
btnB = Key2(6, 'B', ABC3, 'j', root)
btnC1 = Key2(7, 'C1', ABC3, 'k', root)
btnD1 = Key2(8, 'D1', ABC3, 'l', root)
btnE1 = Key2(9, 'E1', ABC3, ';', root)

# -----------------------------------------------------------------------------

root.mainloop()
